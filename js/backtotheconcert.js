//Global variables definition
//Google maps, Overlapping Marker Spiderfier and MarkerClusterer
var map;
var service;
var oms;
var markersArray = [];
var minZoomLevel = 2;
var markerClusterer = null;
var center = new google.maps.LatLng(15, 15); //Default location 

var username;

// Executes when DOM is fully loaded. 
$(document).ready(function() {

    //Textfield options
    $("#username").val('setlist.fm username');
    $("#username").focus(function() {
        $("#username").val("");
    });

    //Map options
    var mapOptions = {
        zoom: minZoomLevel,
        center: center,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    //Declares map
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    oms = new OverlappingMarkerSpiderfier(map);

    // Limit the zoom level
    google.maps.event.addListener(map, 'zoom_changed', function() {
        if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
    });

    //Gets text from textfield and calls start() when the button is clicked
    $("#searchBtn").click(function() {
        start();
    });

    //Gets text from textfield and calls start() when pressing enter on textfield
    $("#username").keypress(function(event) {
        if (event.which == 13) start();
    });
});


// Entry point. Gets the username input by the user and fetches JSON from setlist.fm API

function start() {

    username = document.getElementById('username').value;

    var jsonUrl = "http://api2.setlist.fm/rest/0.1/user/" + username + "/attended.json?callback=?&key=93516000-c2b2-41a0-a10e-5e97f85f9f57";
    //var jsonUrl = "attended.json";

    $.ajax({
        type: 'GET',
        url: jsonUrl,
        async: false,
        jsonpCallback: 'jsonCallback',
        contentType: "application/json",
        dataType: 'jsonp',
        success: function(json) {
            addMarkerfromJson(json);
        },
        error: function(e) {
            console.log("Sorry, there were problems fetching your concerts :(");
        }
    });
}

//Parses setlist.fm JSON information

function addMarkerfromJson(data) {

    var setlists = data.setlists.setlist;
    var concerts = [];

    for (var i = 0, l = setlists.length; i < l; i++) {

        if ($.isEmptyObject(setlists[i].sets)) continue;
        var concert = {
            tour: setlists[i]["@tour"],
            artist: setlists[i].artist["@name"],
            date: setlists[i]["@eventDate"],
            song1: $.isEmptyObject(setlists[i].sets.set[0]) ? "Song not found" : setlists[i].sets.set[0].song[0]["@name"],
            song2: $.isEmptyObject(setlists[i].sets.set[0]) ? "Song not found" : setlists[i].sets.set[0].song[1]["@name"],
            song3: $.isEmptyObject(setlists[i].sets.set[0]) ? "Song not found" : setlists[i].sets.set[0].song[2]["@name"],
            setlistURL: setlists[i].url,
            cityName: setlists[i].venue.city["@name"],
            cityLatitude: setlists[i].venue.city.coords["@lat"],
            cityLongitude: setlists[i].venue.city.coords["@lng"],
            countryName: setlists[i].venue.city.country["@name"],
            venueName: setlists[i].venue["@name"]
        };
        concerts.push(concert);
    }

    for (var j = 0, x = concerts.length; j < x; j++) {
        //Sends data to function in order to build the custom infoWindow
        var infoWindow = createInfoWindow(concerts[j].artist, concerts[j].venueName, concerts[j].tour, concerts[j].date, concerts[j].song1, concerts[j].song2, concerts[j].song3, concerts[j].setlistURL, concerts[j].cityName);
        //Sends data to function that queries Google Places API
        getPlace(concerts[j].venueName, concerts[j].cityName, concerts[j].countryName, concerts[j].cityLatitude, concerts[j].cityLongitude, infoWindow);
    }
}

//Gets venues coordinates from Google Places API. 

function getPlace(venueName, cityName, countryName, cityLatitude, cityLongitude, infoWindowText) {

    var cityCoordinates = new google.maps.LatLng(cityLatitude, cityLongitude);
    var venue = venueName + ' ' + cityName + ' ' + countryName;

    var request = {
        location: cityCoordinates,
        radius: '1000',
        query: venue
    };

    service = new google.maps.places.PlacesService(map);
    service.textSearch(request, function(results, status) {
        if (status == google.maps.places.PlacesServiceStatus.OK) {
            var place = results[0];
            addMarker(place.geometry.location.kb, place.geometry.location.lb, infoWindowText);
        }
    });
}

function callback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        var place = results[0];
        addMarker(place.geometry.location.kb, place.geometry.location.lb);
    }
}

//Adds marker and fills marker array, marker cluster, Overlapping Marker Spiderfier and calls infowindows builder

function addMarker(latitude, longitude, infoWindowText) {
    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(latitude, longitude),
        map: map
    });

    //Links markers with infoWindows
    attachIWindow(marker, infoWindowText);
    //Marker spider
    oms.addMarker(marker);
    //Marker clusters
    markersArray.push(marker);
    refreshMap();
}

//Takes care of clustering markers

function refreshMap() {
    if (markerClusterer !== null) {
        markerClusterer.clearMarkers();
    }
    var zoom = 10;
    markerClusterer = new MarkerClusterer(map, markersArray, {
        maxZoom: zoom
    });
}

//Links markers with custom infoWindows

function attachIWindow(marker, content) {

    var infowindow = new google.maps.InfoWindow({
        maxWidth: 400,
        content: content
    });

    google.maps.event.addListener(marker, 'click', function() {
        map.setCenter(marker.getPosition());
        infowindow.open(map, marker);
    });
}

//Builds HTML for custom infoWindows

function createInfoWindow(artist, venue, tour, date, song1, song2, song3, setlistURL, cityName) {

    var contentStr;
    var queryStr = artist + ' ' + venue + ' ' + date;
    var tourName = (typeof tour != "undefined") ? tour : 'Tour name not found';

    contentStr = '<div class="iwContainer">';
    contentStr += '<div align="left">';
    contentStr += '<h3>' + artist + '</h3><br>';
    contentStr += '<h4>' + tourName + '</h4><br>';
    contentStr += '<h6>' + date + ' @ ' + venue + ', ' + cityName + '</h6></div>';
    contentStr += song1 + '<br>';
    contentStr += song2 + '<br>';
    contentStr += song3 + '<br>';
    contentStr += '<a href="' + setlistURL + '" target="_blank">view full setlist</a><br>';
    contentStr += "<a href='#' onclick='getYoutube(\"" + queryStr + "\", \"search\");'>view Youtube videos</a><br>";
    contentStr += "<a href='#' onclick='getFlickr(\"" + artist + "\", \"" + cityName + "\");'>view Flickr pictures</a><br>";
    contentStr += '</div>';

    return contentStr;
}

//Queries Youtube API for videos related to the current event on the marker/infowindow

function getYoutube(foreign_id, view_type) {

    var max_videos = 2;

    if (!foreign_id) {
        foreign_id = default_id;
        view_type = "id";
    }

    var url = "http://gdata.youtube.com/feeds/api/videos?vq=" + escape(foreign_id) + "&max-results=" + max_videos +
        "&alt=json-in-script&callback=?";

    // Clear the HTML display
    document.getElementById("videos").innerHTML = "";

    // Get the data from the web service and process
    $.getJSON(url, function(data) {
        var results = data.feed.entry;
        if (!results) return;
        for (var i = 0, l = results.length; i < l; i++) {
            // Extract video ID from the videos API ID
            var item = results[i];
            var api_id = item.id.$t;
            api_id.match(/\/(\w+?)$/);
            var id = RegExp.$1;

            var title = item.title.$t;

            // Get first 10 chars of date_taken, which is the date: YYYY-MM-DD
            var date_pub = item.published.$t.substring(0, 10);

            // Collect the authors
            var author_text = "";
            for (var j = 0, x = item.author.length; j < x; j++) {
                var item2 = item.author[j];
                if (author_text) author_text += ", ";
                author_text += '<a href="' + item2.uri.$t + '">' + item2.name.$t + '</a>';
            }

            // Format the HTML for this video
            var text = '<div align="left" class="video">' +
                '<b class="title">' + title + '</b><br/>' +
                'Published: ' + date_pub + ' by ' + author_text + '<br/>' +
                '<object width="120" height="90"><param name="movie" value="http://www.youtube.com/v/' + id + '&hl=en&fs=1"></param><param name="allowFullScreen" value="true"></param><embed src="http://www.youtube.com/v/' + id + '&hl=en&fs=1" type="application/x-shockwave-flash" allowfullscreen="true" width="160" height="110"></embed></object></div>';

            // Now append to the HTML display
            $(text).appendTo("#videos");
        }
    });
}

//Queries flickr API for photos related to the current event on the marker/infowindow

function getFlickr(artist, cityName) {

    var tags = artist + ', ' + cityName;
    var url = "http://api.flickr.com/services/feeds/photos_public.gne?format=json&jsoncallback=?";
    var src;

    $.getJSON(url, {
        tags: tags,
        tagmode: "all",
        format: "json"
    },

    function(data) {
        jQuery('#images').empty();
        $.each(data.items, function(i, item) {
            $("<img/>").attr("src", item.media.m).appendTo("#images");
            document.getElementById('titel').innerHTML = "flickr - pictures of: " + "<b style='color:red;'>" + data.items[i].title + "</b>";
            if (i == 5) return false;
        });
    });
}